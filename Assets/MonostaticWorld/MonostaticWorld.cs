﻿using UnityEngine;
using System.Collections;

public class MonostaticWorld : MonoBehaviour {
	private GameObject level;
	private GameObject snake;
	private Mesh mesh;
	private SnakeLocationHelper snakeLocation = new SnakeLocationHelper();

	// Use this for initialization
	void Start () {
		level = GameObject.Find("RustickSphere");
		mesh = level.GetComponent<MeshFilter>().mesh;

		snakeLocation.SetRelatedGameObject(level);
		snake = GameObject.Find("Snake");
	}

	Vector3 VertexPos(int index) {
		return level.transform.TransformPoint(mesh.vertices[index]);
	}

	void DrawTriangleBorder(int vertexIndex1, int vertexIndex2) {
		int[] triangles = mesh.triangles;
		Debug.DrawLine(VertexPos(triangles[vertexIndex1]), VertexPos(triangles[vertexIndex2]), Color.red);
	}
	
	// Update is called once per frame
	void Update () {
		// Debug world rotation
		if (level) 
		{
			//level.transform.Rotate(Time.deltaTime * 100, 0, 0, Space.World);
		}

		// Update snake location
		snakeLocation.Update();
		if (snake) 
		{
			snake.transform.position = snakeLocation.GetCurrentPosition();
			snake.transform.rotation = Quaternion.LookRotation(snakeLocation.GetCurrentOrientation());

			DrawArrow.ForDebug(snake.transform.position, snakeLocation.GetCurrentOrientation());
		}

		// Debug draw of polygons
		//Vector3[] vertices = mesh.vertices;
		//int[] triangles = mesh.triangles;
		var triangleIndex = snakeLocation.GetCurrentTriangleIndex();
		var i = 3 * triangleIndex;
		DrawTriangleBorder(i, i+1);
		DrawTriangleBorder(i, i+2);
		DrawTriangleBorder(i+1, i+2);

	}
}
