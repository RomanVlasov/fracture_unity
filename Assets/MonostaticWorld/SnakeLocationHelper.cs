﻿using UnityEngine;
using System.Collections;

public class SnakeLocationHelper 
{
	private Vector3 velocity;
	private float velocityMagnitude;
	private Vector3 positionOnTriangle;
	private int currentTriangleIndex;
	private GameObject level;
	private Mesh mesh;
	private TransformationHelper transformationHelper;

	public void SetRelatedGameObject(GameObject relatedLevel)
	{
		level = relatedLevel;
		mesh = level.GetComponent<MeshFilter>().mesh;

		currentTriangleIndex = 5;
		transformationHelper = new TransformationHelper(GetPointsOfTriangle(currentTriangleIndex));

		positionOnTriangle = new Vector3(0, 0, 0);
		velocity = new Vector3(-0.03f, 0.15f, 0.0f);
		velocityMagnitude = velocity.magnitude;
	}

	public Vector3 GetCurrentPosition()
	{
		return level.transform.TransformPoint(transformationHelper.ConvertTriangleCoordToLevel(positionOnTriangle));
	}

	public Vector3 GetCurrentOrientation()
	{
		return level.transform.TransformDirection(transformationHelper.GetNormal());
	}

	public int GetCurrentTriangleIndex()
	{
		return currentTriangleIndex;
	}

	public void Update()
	{
		var nextPosition = positionOnTriangle + velocity * Time.deltaTime;
		if (PointInCurrentTriangle(nextPosition)) 
		{
			//Movement in current triangle
			positionOnTriangle = nextPosition;
		} 
		else 
		{
			//Jumping to another triangle
			Vector3 crossPointOnTriangle;
			float cosAngleCrossing;
			Vector3 crossedSegmentDirection;
			var newTriangleIndex = FindTriangleToJump(out crossPointOnTriangle, out cosAngleCrossing, out crossedSegmentDirection, nextPosition);

			//TODO: Apply new magnitude
			var newMagnitude = (nextPosition - positionOnTriangle).magnitude - (crossPointOnTriangle - positionOnTriangle).magnitude;

			var crossPointOnLevel = transformationHelper.ConvertTriangleCoordToLevel(crossPointOnTriangle);
			var newTransformationHelper = new TransformationHelper(GetPointsOfTriangle(newTriangleIndex));
			positionOnTriangle = newTransformationHelper.ConvertLevelCoordToTriangle(crossPointOnLevel);

			var velocityOnLevel = transformationHelper.ConvertTriangleDirectionToLevel(velocity);

			//Set direction vector in new triangle
			var crossedSegmentOnLevel = transformationHelper.ConvertTriangleDirectionToLevel(crossedSegmentDirection);
			DrawArrow.ForDebug(crossPointOnLevel, crossedSegmentOnLevel);
			var crossedSegmentOnNewTriangle = newTransformationHelper.ConvertLevelDirectionToTriangle(crossedSegmentOnLevel);
			var crossedSegmentDirectionAngle = CalcDirectionAngleOfVector(crossedSegmentOnNewTriangle);


			var newVelocityDirection = CalcVelocityDirectionOnNewTriangle(cosAngleCrossing, 
			                                                              crossedSegmentOnNewTriangle,
			                                                              positionOnTriangle,
			                                                              newTransformationHelper.GetRefPointToJump());

			velocity = Vector3.ClampMagnitude(newVelocityDirection, velocityMagnitude);

			transformationHelper = newTransformationHelper;
			currentTriangleIndex = newTriangleIndex;
		}
	}

	private Vector3 CalcVelocityDirectionOnNewTriangle(float cosAngleOnPreviosTriangle, 
	                                                   Vector3 crossedSegmentDirection, 
	                                                   Vector3 crossedPoint,
	                                                   Vector3 refPoint)
	{
		var cosGamma = crossedSegmentDirection.y / crossedSegmentDirection.magnitude;
		var sinGamma = crossedSegmentDirection.x / crossedSegmentDirection.magnitude;
		var cosPhi = cosAngleOnPreviosTriangle;
		var sinPhi = Mathf.Sqrt(1 - cosPhi * cosPhi);

		var firstVariant = new Vector3();
		firstVariant.x = sinGamma * cosPhi + cosGamma * sinPhi;
		firstVariant.y = cosGamma * cosPhi - sinGamma * sinPhi;

		var testLine = new StraightLine2(crossedPoint, crossedSegmentDirection);
		if (testLine.GetRelativeSideOfPoint(firstVariant) == testLine.GetRelativeSideOfPoint(refPoint)) 
		{
			return firstVariant;
		}

		var secondVariant = new Vector3();
		secondVariant.x = sinGamma * cosPhi - cosGamma * sinPhi;
		secondVariant.y = cosGamma * cosPhi + sinGamma * sinPhi;
		return secondVariant;
	}

	private void ProcessTriangleBorder()
	{
		//PointInCurrentTriangle ();
	}

	private int FindTriangleToJump(out Vector3 crossPointOnTriangle, out float cosAngleCrossing, out Vector3 crossedSegmentDirection, Vector3 nextPosition)
	{
		var points = transformationHelper.GetTriangleCoordsInLocalBasis();
		var line = new StraightLine2(positionOnTriangle, velocity);
		int pointIndex1 = 0, pointIndex2 = 0;
		Vector2 crossPoint;
		if (line.CrossesSegmentAfterPoint(out crossPoint, positionOnTriangle, nextPosition, points[0], points[1])) {
			pointIndex1 = 0;
			pointIndex2 = 1;
		} else
		if (line.CrossesSegmentAfterPoint(out crossPoint, positionOnTriangle, nextPosition, points[1], points[2])) {
			pointIndex1 = 1;
			pointIndex2 = 2;
		} else
		if (line.CrossesSegmentAfterPoint(out crossPoint, positionOnTriangle, nextPosition, points[0], points[2])) {
			pointIndex1 = 0;
			pointIndex2 = 2;
		}

		var nextTriangle = GetConnectedTriangle(mesh.triangles[3 * currentTriangleIndex + pointIndex1], 
		                                        mesh.triangles[3 * currentTriangleIndex + pointIndex2]);

		crossPointOnTriangle = new Vector3();
		crossPointOnTriangle = crossPoint;

		crossedSegmentDirection = points[pointIndex1] - points[pointIndex2];
		cosAngleCrossing = Vector2.Dot(points[pointIndex1] - points[pointIndex2], velocity) / 
			(velocity.magnitude * crossedSegmentDirection.magnitude);

		return nextTriangle;
	}

	private int GetConnectedTriangle(int vertexIndex1, int vertexIndex2)
	{
		for (int i = 0; i < mesh.triangles.Length / 3; i++) 
		{
			if (i == currentTriangleIndex)
			{
				continue;
			}

			var vertexCount = 0;
			for (int j = 3 * i; j < 3 * i + 3; j++)
			{
				if (mesh.vertices[mesh.triangles[j]] == mesh.vertices[vertexIndex1] || 
				    mesh.vertices[mesh.triangles[j]] == mesh.vertices[vertexIndex2])
				{
					vertexCount++;
				}
			}

			if (vertexCount >= 2)
			{
				return i;
			}
		}

		//Do something in this case
		return currentTriangleIndex;
	}

	private Vector3[] GetPointsOfTriangle(int index)
	{
		Vector3[] result = new Vector3[3];
		result[0] = mesh.vertices[mesh.triangles[index * 3]];
		result[1] = mesh.vertices[mesh.triangles[index * 3 + 1]];
		result[2] = mesh.vertices[mesh.triangles[index * 3 + 2]];
		return result;
	}

	private float CalcDirectionAngleOfVector(Vector2 direction)
	{
		return Mathf.Atan2(direction.y, direction.x);
	}
	
	bool PointInCurrentTriangle(Vector3 testPoint)
	{
		var p = transformationHelper.GetTriangleCoordsInLocalBasis();
		//	GetPointsOfTriangle(currentTriangleIndex);
		//var p = new Vector3[3];
		//for (int i = 0; i < p.Length; i++) 
		//{
		//	p[i] = transformationHelper.ConvertLevelCoordToTriangle(tp[i]);
		//}

		float alpha = ((p[1].y - p[2].y)*(testPoint.x - p[2].x) + (p[2].x - p[1].x)*(testPoint.y - p[2].y)) /
			((p[1].y - p[2].y)*(p[0].x - p[2].x) + (p[2].x - p[1].x)*(p[0].y - p[2].y));
		float beta = ((p[2].y - p[0].y)*(testPoint.x - p[2].x) + (p[0].x - p[2].x)*(testPoint.y - p[2].y)) /
			((p[1].y - p[2].y)*(p[0].x - p[2].x) + (p[2].x - p[1].x)*(p[0].y - p[2].y));
		float gamma = 1.0f - alpha - beta;
		return alpha >= 0 && beta >= 0 && gamma >= 0;
	}
}
