using UnityEngine;
using System.Collections;

public class TransformationHelper
{
	private Matrix4x4 transformMatrix;
	private Vector3[] points;
	private Vector3[] pointsInLocalBasis;
	private Vector3[] basis;

	public TransformationHelper(Vector3[] trianglePoints)
	{
		points = new Vector3[trianglePoints.Length];
		for (int i = 0; i < trianglePoints.Length; i++) 
		{
			points[i] = trianglePoints[i];
		}

		////////////////////////////////////////////
		//First, calculate new basis in triangle
		////////////////////////////////////////////
		basis = new Vector3[3];
		basis[0] = (points [2] - points [0]).normalized;
		basis[2] = CalcNormalOfTriangle();
		basis[1] = Vector3.Cross(basis[0], basis[2]).normalized;

		////////////////////////////////////////////
		//Then cache transformatio matrix
		////////////////////////////////////////////
		transformMatrix = new Matrix4x4();
		for(int i = 0; i < basis.Length; i++)
		{
			transformMatrix.SetColumn(i, (Vector4)basis[i]);
		}
		transformMatrix.SetColumn(3, new Vector4(0, 0, 0, 1));

		////////////////////////////////////////////
		//Finally cache points in local basis
		////////////////////////////////////////////
		pointsInLocalBasis = new Vector3[3];
		for (int i = 0; i < 3; i++) 
		{
			pointsInLocalBasis[i] = ConvertLevelCoordToTriangle(points[i]);
		}
	}

	public Vector3 GetNormal()
	{
		return basis[2];
	}

	public Vector3[] GetBasis()
	{
		return basis;
	}

	public Vector3[] GetTriangleCoordsInLocalBasis()
	{
		return pointsInLocalBasis;
	}

	public Vector3 GetRefPointToJump()
	{
		return pointsInLocalBasis[1];
	}

	public Vector3 ConvertLevelCoordToTriangle(Vector3 levelCoord)
	{
		return transformMatrix.MultiplyPoint3x4(levelCoord - points[0]);
	}

	public Vector3 ConvertTriangleCoordToLevel(Vector3 triangleCoord)
	{
		Vector3 result = transformMatrix.inverse.MultiplyPoint3x4(triangleCoord);
		Vector3 shift = points[0];
		return result + shift;
	}

	public Vector3 ConvertTriangleDirectionToLevel(Vector3 triangleDirection)
	{
		return transformMatrix.inverse.MultiplyPoint3x4(triangleDirection);
	}

	public Vector3 ConvertLevelDirectionToTriangle(Vector3 levelDirection)
	{
		return transformMatrix.MultiplyPoint3x4(levelDirection);
	}

	private Vector3 CalcNormalOfTriangle()
	{
		Vector3 vertex1 = points[0];
		Vector3 vertex2 = points[1];
		Vector3 vertex3 = points[2];
		
		Vector3 side1 = vertex2 - vertex1;
		Vector3 side2 = vertex3 - vertex1;
		
		Vector3 normal = Vector3.Cross(side1, side2);
		normal.Normalize();
		return normal;
	}
}
